var fs = require('fs');

var json = JSON.parse(fs.readFileSync('./package.json'));

//paths
var theme = 'wordpress/wp-content/themes/' + json.name + '/';
// Processes
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify-es').default,
    babel = require('gulp-babel'),
    browserSync = require('browser-sync').create(),
    gcmq = require('gulp-group-css-media-queries'),
    svg2png = require('gulp-svg2png'),
    svgSymbols = require('gulp-svg-symbols');
pump = require('pump');

// Paths
var input = theme + 'assets/css/*.scss',
    output = theme + 'build/css',
    maps = theme + 'build/css';

// Options
var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

var autoPrefixerOptions = {
    browsers: ['last 10 versions', '> 5%', 'Firefox ESR'],
    remove: false,
    cascade: false
};

function exceptionLog(error) {
    console.log(error.toString());
    this.emit('end');
}
// Tasks
gulp.task('sass', function () {
    return gulp
        .src(input)
        // Sourcemamps
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions)).on('error', notify.onError(function (error) {
            return 'Problem file -> ' + error.message;
        }))

        // Write the sourcemap
        .pipe(sourcemaps.write())
        // Prefix that shit
        .pipe(autoprefixer())
        .pipe(gulp.dest(output))
        .pipe(browserSync.stream())
        .on('error', exceptionLog);
});

gulp.task('scripts', function () {
    gulp.src([
        // This adds all files except main

        theme + 'assets/js/**/*.js'
    ])

        .pipe(gulp.dest(theme + 'build/js/'));
    browserSync.reload();
});


gulp.task('babel', function () {
    gulp.src(theme + 'assets/js/libs/*.js')
        .pipe(gulp.dest(theme + 'build/js/libs/'));

    gulp
        .src([theme + 'assets/js/**/*.js', '!' + theme + 'assets/js/libs/*.js'])
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest(theme + 'build/js/'));

    browserSync.reload();
});

gulp.task('compress', function () {
    return gulp
        .src(theme + 'assets/js/**/*.js')
        .pipe(uglify().on('error', function (e) {
            console.log('uglify error:');
            console.log(e);
        }))
        .pipe(gulp.dest(theme + 'build/js/'));
});

gulp.task('watch', function () {
    gulp.watch(input, ['sass']);
    gulp.watch(theme + 'assets/js/**/*.js', ['babel']);
    gulp.watch(theme + 'assets/js/*.js', ['babel']);
    gulp.watch(theme + '**/*.php').on('change', browserSync.reload);
    gulp.watch(theme + '*.php').on('change', browserSync.reload);
});

gulp.task('sass-compress', function () {
    return gulp
        .src(input)
        .pipe(sass())
        // .pipe(gcmq())
        .pipe(autoprefixer(autoPrefixerOptions))
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(gulp.dest(output));
});

gulp.task('svg', function () {
    return gulp
        .src(theme + 'assets/img/*.svg')
        .pipe(svg2png())
        .pipe(gulp.dest(theme + 'assets/img/png/'));
});


gulp.task('syncwat', function () {
    browserSync.init({
        open: true,
        proxy: json.name + '.wat',
        watchTask: true,
        browser: ['chrome'],
        port: 3025
    });
});

gulp.task('dev', ['syncwat', 'watch']);
gulp.task('deploy', ['sass-compress', 'babel']);