<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage site
 * @since site 1.0
 */

get_header(); ?>
<? if (have_posts()) : while (have_posts()) : the_post();?>
	
    <main>
        <? the_content(); ?>
        <a href="<? the_permalink(); ?>">Here</a>
    </main>
    
    <?endwhile; endif;?>

<?php get_footer(); ?>
