<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5c16a5bf5119af8ebf5b85ae68abb848
{
    public static $prefixLengthsPsr4 = array (
        'i' => 
        array (
            'inc\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'inc\\' => 
        array (
            0 => __DIR__ . '/../..' . '/inc',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5c16a5bf5119af8ebf5b85ae68abb848::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5c16a5bf5119af8ebf5b85ae68abb848::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
