<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage site
 * @since site 1.0
 */

get_header();
?>

<? the_title(); ?>

<?php get_footer(); ?>
