<?php
/**
 * site functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage site
 * @since 1.0
 */

 // require once the Composer autoload functionality
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
    require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * Initialise all the core classes of the theme
*/
if ( class_exists( 'inc\\init' ) ) {
    inc\init::register_services();
} else {
    exit('no class found');
}

if (defined('FORCE_SSL'))
  add_action('template_redirect', 'force_ssl');

function force_ssl(){

	if ( FORCE_SSL && !is_ssl() )
	{
  		wp_redirect('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 301 );
  		exit();
	}
}

if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}


//include all functional files
foreach (glob(get_template_directory()."/inc/libs/*.php") as $filename)
{
    require_once($filename);
}
foreach (glob(get_template_directory()."/inc/*.php") as $filename)
{
    require_once($filename);
}

if (!function_exists('write_log')) {
    function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}


if ( ! function_exists( 'site_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function site_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/site
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'site' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'site' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	//make sure pages have an excerpt too
	//add_post_type_support( 'page', 'excerpt' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );



	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'main'    	=> __( 'Main Menu', 'site' ),
		'footer' 	=> __( 'Footer Menu', 'site' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	// add_theme_support( 'post-formats', array(
	// 	'aside',
	// 	'image',
	// 	'video',
	// 	'quote',
	// 	'link',
	// 	'gallery',
	// 	'audio',
	// ) );


	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'build/css/editor-style.css' ) );


}
endif;
add_action( 'after_setup_theme', 'site_setup' );



/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function site_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'site' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'site_excerpt_more' );


/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function site_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'site_pingback_header' );



/**
 * Enqueue scripts and styles.
 */
function site_scripts() {

	if(!is_admin())
    {
    	wp_register_script('jquery', 'https://code.jquery.com/jquery-3.2.1.min.js', array(), null, true);

    	// Load our main stylesheet.
		foreach (glob(get_stylesheet_directory().'/build/css/*.*') as $file) {
			if(is_file($file))
			{
				$file_url = str_replace(get_stylesheet_directory(), get_theme_file_uri(), $file);
				$file_name = basename($file);
				if($file_name == 'style.css')
				{
					$file_name = 'default';
				}
				wp_enqueue_style( $file_name, $file_url , false, filemtime( $file ) );
			}
		}

		// Load other stylesheets.
		foreach (glob(get_stylesheet_directory().'/build/css/**/*.*') as $file) {
			if(is_file($file))
			{
				$file_url = str_replace(get_stylesheet_directory(), get_theme_file_uri(), $file);
				$file_name = basename($file);
				if($file_name == 'style.css')
				{
					$file_name = 'default';
				}
				wp_enqueue_style( $file_name, $file_url , false, filemtime( $file ) );
			}
		}

		//load all libs
		foreach (glob(get_stylesheet_directory().'/build/js/libs/*.*') as $file) {
			if(is_file($file))
			{
				$file_url = str_replace(get_stylesheet_directory(), get_theme_file_uri(), $file);
				$file_name = basename($file);
				wp_enqueue_script($file_name,$file_url,array('jquery'),filemtime( $file ) ,true);
			}
		}

		//load individual js
		foreach (glob(get_stylesheet_directory().'/build/js/*.*') as $file) {
			if(is_file($file))
			{
				$file_url = str_replace(get_stylesheet_directory(), get_theme_file_uri(), $file);
				$file_name = basename($file);
				wp_enqueue_script($file_name,$file_url,array('jquery'),filemtime( $file ) ,true);
			}
		}
    }
	
	// Localize for script
	$script_data[ 'debug' ] = WP_DEBUG;
	$script_data[ 'nonce' ] = wp_create_nonce( 'site-super-secure-shmnbcedfhsod' );
	$script_data[ 'siteUrl' ] = get_site_url();
	$script_data[ 'themeUrl' ] = str_replace(get_site_url(), '', get_template_directory_uri().'/');
	$script_data[ 'pageTitleSub' ] = ' — '.get_bloginfo( 'name' );



	//only need to do this once for the first script that needs it
	wp_localize_script('site-debug', 'wpAPIData', $script_data );

}
add_action( 'wp_enqueue_scripts', 'site_scripts' );

add_filter( 'script_loader_tag', 'defer_scripts', 10, 3 );
function defer_scripts( $tag, $handle, $src )
{
    if( strpos($handle, 'site-') === false ) return $tag;
    return str_replace('></script>', ' defer="defer"></script>', $tag);
}

add_filter( 'wp_default_scripts', 'site_remove_jquery_migrate' );
function site_remove_jquery_migrate( &$scripts)
{
    if(!is_admin())
    {
        $scripts->remove( 'jquery');
    }
}

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

function custom_disable_mce_buttons( $opt ) {
    $opt['block_formats'] = 'Paragraph=p;Heading 1=h1;Heading 2=h2';
    return $opt;
}
add_filter('tiny_mce_before_init', 'custom_disable_mce_buttons');   