<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\utils;

class acf {
    function register() {
        add_action( 'wp_ajax_fetch_page_image',         [ $this, 'fetch_page_image' ] );
        // add_action( 'wp_ajax_nopriv_fetch_page_image',  [ $this, 'fetch_page_image' ] );

        add_action( 'wp_ajax_fetch_thumbnail_dom_color',         [ $this, 'fetch_thumbnail_dom_color' ] );
        // add_action( 'wp_ajax_nopriv_fetch_thumbnail_dom_color',  [ $this, 'fetch_thumbnail_dom_color' ] );

        // add_action('current_screen', [ $this, 'get_current_screen' ]);
    }

    public function fetch_page_image() {
        $post_id = $_POST['post'];
        $image = get_the_post_thumbnail_url($post_id, 'post-thumbnail');
        $dominant_color = get_field('dominant_color', get_post_thumbnail_id($post_id));

        $html = '<div class="default-dominant-color" style="padding: 20px; display: inline-block; background-color: '.$dominant_color.'">
            <img width="150" height="150" src="'.$image.'" />
            <span style="display: inline-block; margin-top: 10px;">Dominant Color: <br />'.$dominant_color.'</span>
        </div>';
            
        wp_send_json_success([
            'image_thumbnail'   => $image,
            'dominant_color'    => $dominant_color,
            'html'              => $html
        ]);
    }

    public function fetch_thumbnail_dom_color() {
        $dominant_color = get_field('dominant_color', $_POST['post']);

        wp_send_json_success([
            'dominant_color' => $dominant_color
        ]);
    }

    public function get_current_screen() {
        //fb(get_current_screen());
    }
}
