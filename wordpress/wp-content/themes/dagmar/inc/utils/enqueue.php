<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\utils;

class enqueue
{

    public function register() 
    {
        add_action( 'wp_enqueue_scripts', [ $this, 'load_scripts' ]);
        add_filter( 'script_loader_tag', [ $this, 'defer_scripts' ], 10, 3 );
        add_filter( 'wp_default_scripts', [ $this, 'remove_jquery_migrate' ] );

        add_action( 'wp_head', [ $this, 'pingback_header' ] );
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
    }

    public function load_scripts() 
    {
        if (!is_admin()) {
            $this->enqueue_styles();
            $this->enqueue_js();
            $this->create_js_object();
        }
    }

    public function create_js_object() 
    {
        // Localize for script
        $locations = new \inc\customposts\locations;
        $script_data = [
            'googleAPIKey' => 'AIzaSyAhfUFABK3NEgKEvH86Z4WDhVPLfBaMXxI',
            'debug'   => WP_DEBUG,
            'nonce'   => wp_create_nonce( 'site-super-secure-shmnbcedfhsod' ),
            'rest_nonce' =>  wp_create_nonce( 'wp_rest' ),
            'ajaxUrl' => admin_url( 'admin-ajax.php'),
            'siteUrl' => get_site_url(),
            'themeUrl' => str_replace(get_site_url(), '', get_template_directory_uri().'/'),
            'pageTitleSub' => ' — '.get_bloginfo( 'name' ),
            'locations' => $locations->get_location_data()
        ];

        //only need to do this once for the first script that needs it
        wp_localize_script('ajaxify.js', 'wpAPIData', $script_data );
    }


    public function enqueue_js() 
    {
        // Enqueue cdns
        wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), null, true);

        // Load our scripts
        $files = array_merge( 
            glob(get_stylesheet_directory().'/build/js/*.*'), 
            glob(get_stylesheet_directory().'/build/js/**/*.*') 
        );

        foreach ($files as $file) {
            if(is_file($file))
			{
				$file_url = str_replace(get_stylesheet_directory(), get_theme_file_uri(), $file);
                $file_name = basename($file);
                $dir = basename(dirname($file_url));

                if ($dir !== 'admin') {
                    $load_after = [ 'jquery' ];
                    if ($file_name !== 'global.js') $load_after[] = 'global.js';
    
                    wp_enqueue_script( $file_name, $file_url, $load_after, filemtime( $file ), true);
                }
			}
        }
    }

    public function enqueue_styles() 
    {
        // enqueue any external css
        wp_enqueue_style('custom-font', 'https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i,700|Open+Sans:300,400,600,700', false );

        $files = array_merge(
            glob(get_stylesheet_directory().'/build/css/*.*'),
            glob(get_stylesheet_directory().'/build/css/**/*.*'),
            glob(get_stylesheet_directory().'/build/css/**/**/*.*')
        );

        foreach ($files as $file) {
            if(is_file($file)) {
                $file_url = str_replace(get_stylesheet_directory(), get_theme_file_uri(), $file);
                $file_name = basename($file);
                $dir = basename(dirname($file_url));
                
                if ($dir !== 'admin') {
                    $load_after = [];
        
                    if($file_name == 'style.css') {
                        $file_name = 'default';
                    } else {
                        $load_after[] = 'default';
                    };
                    
                    wp_register_style( $file_name, $file_url, false, filemtime( $file ) );
                    wp_enqueue_style( $file_name );
                }
            }
        }
    }

    public function defer_scripts( $tag, $handle, $src )
    {
        if( strpos($handle, 'screen-yorkshire-') === false ) return $tag;
        return str_replace('></script>', ' defer="defer"></script>', $tag);
    }

    public function remove_jquery_migrate( &$scripts )
    {
        if (!is_admin()) $scripts->remove('jquery');
    }


    public function pingback_header() 
    {
        if ( is_singular() && pings_open() ) {
            printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
        }
    }
}