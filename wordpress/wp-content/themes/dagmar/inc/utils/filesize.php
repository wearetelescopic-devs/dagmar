<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\utils;

class filesize {
    public static function get_file_details($file) {
        // TODO: check if saved it to the database -> skip get file type and size if so

        $file_type = self::get_file_type($file['mime_type']);
        $file_size = self::get_file_size($file['url']);

        // TODO: if we didnt save it before, before returning, save to the database

        return $file_size . ' ' . pathinfo($file['url'])['extension'];
    }

    private static function get_file_type($mime_type) {
        return strtoupper(explode('/', $mime_type)[1]);
    }

    private static function get_file_size($url) {
        $headers = get_headers($url, 1);
        //wtf
        if(isset($headers['Content-Length'])) {
            
            $file_size = $headers['Content-Length'];
    
    
            return self::formatbytes($file_size, 1);
        }
        else {
            return false;
        }
    }

    private static function formatbytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow)); 

        return round($bytes, $precision). strtolower($units[$pow]); 
    }
}