<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\utils;

class googlemap {

    public function __construct(){
        add_shortcode("googlemap", array($this, 'fn_googleMaps'));
    }

    public function fn_googleMaps($atts, $content = null) {
        
        $atts = shortcode_atts( array(
            'gmap_query' => "",
            'lat' => "",
            'long' => "",
        ),$atts);
        return '<iframe class="location-map" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q='.trim(esc_attr($atts['gmap_query'])).'&key=AIzaSyBQ51IM4Vn66fXpe-YgpmKZj1Q0xPLfEdg" allowfullscreen>
                </iframe>';

    
    //return "<img src=https://www.google.com/maps/@". esc_attr($atts[lat]) . "," . esc_attr($atts[long]). ",14z></a>";
    }

}
