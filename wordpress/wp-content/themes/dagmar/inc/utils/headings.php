<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\utils;

class headings {

    static function title_widows($text) {

        $parts = explode(' ', $text);
        if (sizeof($parts) < 4) {
            // if there are 3 parts, the nbsp may leave an orphan at the top instead
            return $text;
        }

        $p2 = array_pop($parts);
        $p1 = array_pop($parts);

        return implode(' ', $parts) . ' <span class="no-break">' . $p1 . ' ' . $p2 . '</span>';
        
    }

}
