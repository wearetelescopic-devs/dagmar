<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\admin;

class optionspage
{
    public function register() {
        if( function_exists('acf_add_options_page') ) {
        
        	acf_add_options_page(array(
        		'page_title' 	=> 'site Information',
        		'menu_title'	=> 'site Information',
        		'menu_slug' 	=> 'sy-information',
        		'capability'	=> 'edit_posts',
        		'redirect'		=> false
        	));
        
		}
		
        if( function_exists('acf_add_options_sub_page') ) {
        
        	acf_add_options_sub_page(array(
        		'page_title' 	=> 'Members Options',
				'menu_title'	=> 'Members Options',
				'parent'     	=> 'edit.php?post_type=sy_members',
        		'menu_slug' 	=> 'sy-members-options',
        		'capability'	=> 'edit_posts',
        		'redirect'		=> false
        	));
        
        	acf_add_options_sub_page(array(
        		'page_title' 	=> 'Media Options',
				'menu_title'	=> 'Media Options',
				'parent'     	=> 'upload.php',
        		'menu_slug' 	=> 'media-options',
        		'capability'	=> 'edit_posts',
        		'redirect'		=> false
        	));
        
        }
    }
}