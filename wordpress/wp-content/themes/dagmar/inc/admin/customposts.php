<?
/**
 * @package Wordpress
 * @subpackage site 
*/
namespace inc\admin;

class customposts
{
    function register() {
        add_action('init', [ $this, 'register_custom_posts' ] );

        add_action('init', [ $this, 'add_rewrite_rules']);

        add_filter( 'custom_menu_order', [ $this, 'custom_menu_order' ], 10, 1 );
        add_filter( 'menu_order', [ $this, 'custom_menu_order' ], 10, 1 );

        add_action( 'admin_menu', [ $this, 'edit_news_menu' ] );
        add_filter( 'post_type_link', [$this,'wpa_course_post_link'], 1, 3 );
        // Manage columns for custom taxonomies and custom post types
        add_action('admin_init', [$this, 'manage_custom_taxonomies_columns']);
        add_filter( 'manage_sy_location_type_custom_column', [$this, 'manage_location_type_column'], 10, 3 );
    }

    public function register_custom_posts() {
        $this->change_post_to_custom();

        // Add the new custom post types and the related toxonomies
        $this->new_locations();
        $this->new_team_members();
        $this->new_faqs();
        $this->new_case_studies();
        $this->new_credits(); 

        if(WP_DEBUG == true)
        {
            flush_rewrite_rules(true);
        }
    }

    public function custom_menu_order( $menu_ord ) {
        // fb($menu_ord);
        if ( !$menu_ord ) return true;

        return [
            'index.php',                            // Dashboard
            'separator1',                           // First separator
            'edit.php?post_type=page',              // Pages
            'edit.php?post_type=sy_locations',      // Locations
            'edit.php?post_type=sy_case_studies',   // Case Studies
            'edit.php?post_type=sy_credits',        // Credits
            'edit.php?post_type=sy_faqs',           // FAQs
            'edit.php',                             // News
            'edit.php?post_type=sy_members',        // Team Members
            'separator2',                           // Second separator
            'sy-information',                       // Options Page
            'separator-last',                       // Last separator
            'upload.php',                           // Media
            'themes.php',                           // Appearance
            'plugins.php',                          // Plugins
            'users.php',                            // Users
            'tools.php',                            // Tools
            'options-general.php',                  // Settings
        ];
    }

    public function change_post_to_custom() {
        // change the basic post type to 'news' instead of 'post'
        global $wp_post_types;
        $wp_post_types['post']->menu_icon = 'dashicons-align-left';
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'News';
        $labels->singular_name = 'News';
        $labels->add_new = 'Add News';
        $labels->add_new_item = 'Add News';
        $labels->edit_item = 'Edit News';
        $labels->new_item = 'News';
        $labels->view_item = 'View News / Event';
        $labels->search_items = 'Search News';
        $labels->not_found = 'No News found';
        $labels->not_found_in_trash = 'No News found in Trash';
        $labels->all_items = 'All News';
        $labels->menu_name = 'News';
        $labels->name_admin_bar = 'News';
    }

    public function edit_news_menu() {
        remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
        global $submenu;
        $submenu['edit.php'][16][0] = 'News Tags';
    }

    function wpa_course_post_link( $post_link, $id = 0 ){
        $post = get_post($id);  
        if ( is_object( $post ) && $post->post_type == 'sy_locations' ){
            $terms = wp_get_object_terms( $post->ID, 'sy_location_type' );
            if( $terms ){
                return str_replace( '%sy_location_type%' , $terms[0]->slug , $post_link );
            }
        }
        return $post_link;  
    }

    

    public function new_locations() {
        register_post_type( 'sy_locations',
            [
                'labels' => $this->add_post_labels('Location'),
                'public' 		=> true,
                'has_archive' 	=> false,
                'hierarchical' 	=> false,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'menu_icon'           => 'dashicons-admin-multisite',
                'supports' => array('title', 'thumbnail', 'custom-fields'),
                'capability_type' => 'post',
                'taxonomies' => [ 'sy_location_type' ],
                'rewrite' => [
                    'slug' => 'filming-in-yorkshire/locations-production/%sy_location_type%',
                    'with_front' => false,
                ]
            ]
        );

        register_taxonomy('sy_location_type', 'sy_locations', [
            'hierarchical'    	=> false,
            'labels'            => $this->add_taxonomy_labels('Location Type'),
            'query_var'       	=> true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'has_archive'       => true,
            'show_in_nav_menus' => true,
            'meta_box_cb'       => false,
            'public'            => true,
            'rewrite'           => array(
                                'with_front'    => false,
                                'slug' => 'filming-in-yorkshire/locations-production'
                                    )
        ]);
    }

    public function new_team_members() {
        register_post_type( 'sy_members',
            [
                'labels' => $this->add_post_labels('Team Member'),
                'public' 		=> false,
                'has_archive' 	=> false,
                'hierarchical' 	=> false,
                'show_ui'             => true,
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'menu_icon'           => 'dashicons-admin-users',
                'supports' => array('title', 'thumbnail', 'editor'),
                'capability_type' => 'post'
            ]
        );
    }

    public function new_faqs() {
        register_post_type( 'sy_faqs',
            [
                'labels' => $this->add_post_labels('FAQ'),
                'publicly_queryable'  => false,
                'has_archive' 	=> false,
                'hierarchical' 	=> false,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'menu_icon'           => 'dashicons-editor-help',
                'supports' => array('title', 'editor', 'thumbnail'),
                'capability_type' => 'post'
            ]
        );

        register_taxonomy( 'sy_faq_tag', 'sy_faqs', [
            'hierarchical'    	=> false,
            'labels'            => $this->add_taxonomy_labels('FAQ Tag'),
            'query_var'       	=> 'faq_tag',
			'show_ui'           => true,
			'show_admin_column' => true,
			'has_archive'       => false,
			// 'show_in_nav_menus' => true
        ]);
    }

    public function new_case_studies() {
        register_post_type( 'sy_case_studies',
            [
                'labels' => $this->add_post_labels('Case Study', 'Case Studies'),
                'publicly_queryable'  => true,
                'public' => true,
                'has_archive' 	=> false,
                'hierarchical' 	=> false,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'menu_icon'           => 'dashicons-megaphone',
                'supports' => array('title', 'thumbnail'),
                'capability_type' => 'post',
                'rewrite' => [
                    'slug' => 'case-studies',
                    'with_front' => false
                ]
            ]
        );

        register_taxonomy( 'sy_case_study_tag', 'sy_case_studies', [
            'hierarchical'    	=> false,
            'labels'            => $this->add_taxonomy_labels('Case Study Tag'),
            'query_var'       	=> 'case_study_tag',
			'show_ui'           => true,
			'show_admin_column' => true,
			'has_archive'       => false,
			// 'show_in_nav_menus' => true
        ]);
    }

    public function new_credits() {
        register_post_type( 'sy_credits',
            [
                'labels' => $this->add_post_labels('Credit'),
                'publicly_queryable'  => true,
                'public' => true,
                'has_archive' 	=> false,
                'hierarchical' 	=> false,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'show_in_rest'        => true,
                'menu_icon'           => 'dashicons-heart',
                'supports' => array('title', 'editor', 'thumbnail'),
                'capability_type' => 'post',
                'rewrite' => [
                    'slug' => 'funding/credits',
                    'with_front' => false
                ]
            ]
        );

        register_taxonomy( 'sy_credit_tag', 'sy_credits', [
            'hierarchical'    	=> false,
            'labels'            => $this->add_taxonomy_labels('Credit Tag'),
            'query_var'       	=> 'credit_tag',
			'show_ui'           => true,
			'show_admin_column' => true,
            'has_archive'       => false,
            'show_in_rest'      => true
			// 'show_in_nav_menus' => true
        ]);
    }

    private function add_post_labels($post_type, $plural=false) {
        if (!$plural) {
            $plural = $post_type.'s';
        }
        return [
            'name' => __($plural),
            'singular_name' => __($post_type),
            'add_new' => __('Add new '.$post_type),
            'add_new_item' => __('Add a new '.$post_type),
            'view_item' => __('View '.$post_type),
            'edit_item' => __('Edit '.$post_type),
            'search_items' => __('Search '.$plural),
            'not_found' => __('No '.$plural.' found'),
            'not_found_in_trash' => __('No '.$plural.' found in trash')
        ];
    }

    private function add_taxonomy_labels($taxonomy, $plural=false) {
        if (!$plural) {
            $plural = $taxonomy.'s';
        }

        return [
            'name' => _x( $plural, 'taxonomy general name' ),
            'singular_name' => _x( $taxonomy, 'taxonomy singular name' ),
            'all_items' => __( 'All '.$plural ),
            'edit_item' => __( 'Edit '.$taxonomy ),
            'view_item' => __( 'View '.$taxonomy ),
            'update_item' => __( 'Update '.$taxonomy ),
            'add_new_item' => __( 'Add New '.$taxonomy ),
            'new_item_name' => __( 'New '.$taxonomy.' Name' ),
            'search_items' => __( 'Search '.strtolower($plural) ),
            'separate_items_with_commas' => __( 'Separate '.strtolower($plural).' with commas' ),
            'add_or_remove_items' => __( 'Add or remove '.strtolower($plural) ),
            'choose_from_most_used' => __( 'Choose from the most used '.strtolower($plural) ),
            'not_found'=> __( 'No '.strtolower($plural).' found.'),
            'back_to_items' => __( '← Back to '.strtolower($plural) )
        ];
    }

    public function manage_custom_taxonomies_columns() {
        add_filter('manage_edit-sy_location_type_columns', [$this, 'add_location_type_column_content'],10,3);
    }

    public function add_location_type_column_content($columns) {
        $new_column = ['location_type_icon' => __('Location Type Icon')];
        unset( $columns['description'] );

        return array_merge($columns, $new_column);
    }

    public function add_rewrite_rules() {

        add_rewrite_tag('%f_app_id%', '([0-9]+)');
        add_rewrite_rule( '^funding/stage-2/([0-9]+)/?$', 'index.php?pagename=funding/stage-2&f_app_id=$matches[1]', 'top' );
    }

    public function manage_location_type_column( $content, $column_name, $term_id ) {
        if ( 'location_type_icon' == $column_name ) {
            $icon = get_term_meta($term_id, 'icon', true);
            if ($icon) {
                $content = wp_get_attachment_image($icon, '', true);
            } else {
                $content = 'N / A';
            }
        }

        return $content;  
    }
}