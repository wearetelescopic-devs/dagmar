<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\admin;

class ajaxify
{
    function register() {
        add_filter( 'wpseo_breadcrumb_links', [ $this, 'wpseo_breadcrumb_links' ] );
    }

    public static function is_ajax()
    {
        if(isset($_GET['ajax']) && $_GET['ajax'] == 1)
        {
            return true;
        }
        return false;
    }

    public function wpseo_breadcrumb_links( $links ) {
        if ( is_single() ) {

            global $wp;
            global $post;

            $current_url = home_url(add_query_arg(array(),$wp->request));
            $parent = str_replace($post->post_name, '', str_replace(site_url(), '', $current_url));


            $up = get_page_by_path($parent);
            if ($up) {

                array_splice( $links, -1, 0, array(
                    [
                        'text' => $up->post_title,
                        'url' => get_the_permalink($up->ID),
                        'allow_html' => true
                    ]
                ));

            }
        }
        elseif (is_tax() ) {
            global $wp;
            global $post;
            $current_url = home_url(add_query_arg(array(),$wp->request));
            
            $queried_object = get_queried_object();

            $parent = str_replace($queried_object->slug, '', str_replace(site_url(), '', $current_url));

            $up = get_page_by_path($parent);
            if ($up) {

                array_splice( $links, -1, 0, array(
                    [
                        'text' => 'Locations',
                        'url' => get_the_permalink($up->ID),
                        'allow_html' => true
                    ]
                ));

            }
        }

     
        return $links;
    }
}