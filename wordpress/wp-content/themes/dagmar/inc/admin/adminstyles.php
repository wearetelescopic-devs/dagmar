<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\admin;

class adminstyles
{
    function register() {
        //hide the admin bar in the frontend
        add_filter('show_admin_bar', '__return_false');

        if(!is_admin() && !$GLOBALS['pagenow'] === 'wp-login.php') return;
        
        add_action( 'after_setup_theme',                [ $this, 'admin_setup'] );
        
        add_action( 'admin_enqueue_scripts',            [ $this, 'load_admin_scripts' ] );
        add_action( 'acf/input/admin_enqueue_scripts',  [ $this, 'add_acf_admin_js']);

        add_filter( 'get_user_option_admin_color', 	    [ $this, 'change_admin_color'] );
        add_action( 'admin_head', 					    [ $this, 'admin_color_scheme'] );

        //allow svg uploads
        add_filter( 'upload_mimes', 				    [ $this, 'cc_mime_types'] );
        add_filter( 'wp_check_filetype_and_ext',        [ $this, 'check_file_and_ext' ], 100, 4 );

        //replace login screen logo
		add_action( 'login_enqueue_scripts', 		    [ $this, 'login_logo'] );
        add_filter( 'login_headerurl', 				    [ $this, 'loginlogo_url'] );        
    }

    public function admin_setup() {
	   
    }
    
    public function manage_columns( $columns ) {
		if(!$this->show_comments)
		{
			unset($columns['comments']);
		}
  		return $columns;
    }
    
    public function change_admin_color($result) {
	    return 'default';
    }
    
    function admin_color_scheme() {
       global $_wp_admin_css_colors;
	   $_wp_admin_css_colors = 0;
    }

    public function load_admin_scripts() {
        if (is_admin()) {
            $this->add_admin_css();
            $this->add_admin_js();
        }
    }
    
    public function add_admin_css() {
        $files = glob(get_template_directory().'/build/css/admin/*.css');

        foreach ($files as $file) {
            if(is_file($file)) {
				$file_url = str_replace(get_stylesheet_directory(), get_theme_file_uri(), $file);
                $file_name = basename($file);

                wp_register_style( $file_name, $file_url, false, filemtime( $file ) );
                wp_enqueue_style( $file_name );
			}
        }
    }
    
    public function add_admin_js() {
        $files = glob(get_template_directory().'/build/js/admin/*.js');
        foreach ($files as $file) {
            if(is_file($file)) {

                $file_url = str_replace(get_stylesheet_directory(), get_theme_file_uri(), $file);
                $file_name = basename($file);

                wp_enqueue_script( $file_name, $file_url, ['jQuery'], filemtime( $file ), true);
			}
        }
    }

    public function add_acf_admin_js() {
        wp_enqueue_script( 'acf-js', get_template_directory_uri() . '/build/js/admin/acf/acf-fields.js', [], '1.0.0', true );
    }
    
    public function login_logo() { ?>
	    <style type="text/css">
	        body.login div#login h1 a {
	            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-black.svg);
	            padding-bottom: 0;
	            width: 58%;
	            margin-bottom: 0;
	            background-size: 100%;
	        }
	        
	    </style>
    <?php }
    
    public function loginlogo_url($url) {
	  return get_site_url();
    }
    
    public function cc_mime_types( $mimes ){
	  $mimes['svg'] = 'image/svg+xml';
	  $mimes['svgz'] = 'image/svg+xml';
	  return $mimes;
    }
    
    public function check_file_and_ext($filetype_ext_data, $file, $filename, $mimes) {
        if ( substr($filename, -4) === '.svg' ) {
        $filetype_ext_data['ext'] = 'svg';
        $filetype_ext_data['type'] = 'image/svg+xml';
        }
        return $filetype_ext_data;
    }
}