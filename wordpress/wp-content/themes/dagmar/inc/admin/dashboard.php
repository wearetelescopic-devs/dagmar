<?
/**
 * @package Wordpress
 * @subpackage site 
*/
namespace inc\admin;

class dashboard
{
    private $show_comments = false;
    private $show_posts = true;

    public function register() {
        if(!is_admin() && !$GLOBALS['pagenow'] === 'wp-login.php') return;

        add_action('admin_head',                    [ $this, 'RemoveAddMediaButtonsForNonAdmins' ]);
        add_filter('tiny_mce_before_init',          [ $this, 'custom_disable_mce_buttons' ]);   
        add_filter('mce_buttons',                   [ $this, 'tinymce_buttons' ] );
        
        // ADD NEW STUFF
        add_filter('excerpt_more',                  [ $this, 'excerpt_more' ] );
        add_action('admin_menu', 			        [ $this, 'menu_pages' ]); //register custom menu pages
        
        // REMOVE UNWANTED STUFF
        add_action('wp_dashboard_setup',            [ $this, 'remove_dashboard_widget' ] );
        add_action('admin_init',                    [ $this, 'remove_unwanted_columns' ] );
        add_action('admin_menu', 			        [ $this, 'remove_admin_menu_items' ] );
        add_action('admin_menu',			        [ $this, 'remove_post_metaboxes'] );
        add_action('add_meta_boxes', 		        [ $this, 'remove_meta_boxes'], 999 ); 
        add_action('admin_bar_menu', 		        [ $this, 'remove_wp_nodes' ], 999 ); //remove from the "new" menu bar item
        add_filter('contextual_help', 				[ $this, 'remove_help_tabs'], 999, 3 ); // remove help tab

		add_filter('admin_footer_text',             '__return_empty_string', 11 ); //remove the wordpress footer
        add_filter('update_footer',                 '__return_empty_string', 11 ); //remove the wordpress footer
		add_filter('admin_footer_text',             [ $this, 'remove_footer_admin' ] ); //disable appearance options for non admins
        add_filter('screen_options_show_screen',    '__return_false' ); //hide screen options
    }

    public function menu_pages() {

    }

    public function remove_unwanted_columns() {
	   	if(!$this->show_comments)
		{
			add_filter( 'manage_posts_columns' , [$this, 'manage_columns']);
			add_filter( 'manage_pages_columns' , [$this, 'manage_columns']);
		}
    }
        
    public function remove_dashboard_widget() {
	    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
    } 

    public function remove_admin_menu_items() {
		$remove_menu_items = array();
		if(!$this->show_comments)
		{
			$remove_menu_items[] = __('Comments');
		}
		if(!$this->show_posts)
		{
			$remove_menu_items[] = __('Posts');
		}
		global $menu;
		end ($menu);
		while (prev($menu)){
			$item = explode(' ',$menu[key($menu)][0]);
			if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
			unset($menu[key($menu)]);}
		}
    }
    
    public function remove_post_metaboxes() {
	  	// remove_meta_box( 'categorydiv','post','normal' );
		// remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
    }
    
    function remove_meta_boxes() 
	{
		if(!$this->show_comments)
		{
		    remove_meta_box('commentsdiv', 'product', 'normal');
		    remove_meta_box('commentsdiv', 'post', 'normal');
		}
		remove_meta_box('postcustom', 'post', 'normal');
		remove_meta_box('slugdiv', 'post', 'normal');
		remove_meta_box('categorydiv', 'post', 'side');
	    // remove_meta_box('postimagediv', 'page', 'side');
	}

    function remove_help_tabs( $old_help, $screen_id, $screen ){
	    $screen->remove_help_tabs();
	    return $old_help;
	}

	public function manage_columns( $columns ) {
		if(!$this->show_comments)
		{
			unset($columns['comments']);
		}
  		return $columns;
	}


    public function RemoveAddMediaButtonsForNonAdmins(){
        remove_action( 'media_buttons', 'media_buttons' );
    }

    public function excerpt_more($link) {
        if ( is_admin() ) {
            return $link;
        }

        $link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
            esc_url( get_permalink( get_the_ID() ) ),
            /* translators: %s: Name of current post */
            sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'safarikid' ), get_the_title( get_the_ID() ) )
        );
        return ' &hellip; ' . $link;
    }

    public function custom_disable_mce_buttons( $opt ) {
        $opt['block_formats'] = 'Paragraph=p;Heading 1=h1;Heading 2=h2';
        return $opt;
    }

    public function tinymce_buttons( $buttons ) {
        //Remove the text color selector
        $remove = 'blockquote'; //default blockquote button

        //Find the array key and then unset
        if ( ( $key = array_search( $remove, $buttons ) ) !== false )
        unset( $buttons[$key] );

        return $buttons;
    }

    public function remove_wp_nodes() //remove from the NEW bar
	{
	    global $wp_admin_bar;   
	    $wp_admin_bar->remove_menu('wp-logo');
	    if(!$this->show_comments)
		{
			$wp_admin_bar->remove_menu('comments'); 
		}
	    if(!$this->show_posts)
		{
			$wp_admin_bar->remove_menu('post'); 
		}
    }
    
    // Custom appearance menu
	public function remove_footer_admin () {
	    echo "<?php
	  //  Using jQuery: How to allow Editors to edit only Menus (or more!)  [by Chris Lemke aka PrettySickPuppy|gmail]

	  if ( is_user_logged_in() ) { // This IF may be redundant, but safe is better than sorry...
	    if ( current_user_can('edit_theme_options') && !current_user_can('manage_options') ) { // Check if non-Admin
	?>
	      <script>
	    jQuery.noConflict();
	    jQuery(document).ready(function() {
	      //  Comment out the line you WANT to enable, so it displays (is NOT removed).
	      //  For example, the jQuery line for MENUS is commented out below so it's not removed.

	      // THEMES:  If you want to allow THEMES, also comment out APPEARANCE if you want it to display Themes when clicked. (Default behaviour)
	      jQuery('li#menu-appearance.wp-has-submenu li a[href=\"themes.php\"]').remove();
	      jQuery('li#menu-appearance.wp-has-submenu a.wp-has-submenu').removeAttr(\"href\");

	      //CUSTOMIZE
	      jQuery('li#menu-appearance.wp-has-submenu li a[href\^=\"customize.php\"]').remove();

	      // WIDGETS:
	      jQuery('li#menu-appearance.wp-has-submenu li a[href=\"widgets.php\"]').remove();
	      jQuery('li#menu-appearance.wp-has-submenu li a[href=\"theme-editor.php\"]').remove();

	      // MENUS:
	      //jQuery('li#menu-appearance.wp-has-submenu li a[href=\"nav-menus.php\"]').remove();

	      // BACKGROUND:
	      jQuery('li#menu-appearance.wp-has-submenu li a[href=\"themes.php?page=custom-background\"]').remove();
	    });
	      </script>
	<?php
	    } // End IF current_user_can...
	  } // End IF is_user_logged_in...
	?>";
	}
}