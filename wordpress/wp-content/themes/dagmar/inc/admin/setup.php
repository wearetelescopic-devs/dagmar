<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\admin;

class setup
{
    public function register() {
        add_action( 'after_setup_theme', [ $this, 'setup' ] );
        if (defined('FORCE_SSL')) add_action('template_redirect', [ $this, 'force_ssl' ]);
    }

    public function setup() {
        $this->make_available_for_translation();
        $this->do_theme_supports();
        $this->add_editor_stylesheet();
        $this->register_nav_menus();
    }

    public function make_available_for_translation() {
        /*
        * Make theme available for translation.
        * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/safarikid
        * If you're building a theme based on Twenty Seventeen, use a find and replace
        * to change 'safarikid' to the name of your theme in all the template files.
        */
        load_theme_textdomain( 'screen-yorkshire' );
    }

    public function do_theme_supports() {
        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );
        
        /*
        * Let WordPress manage the document title.
        * By adding theme support, we declare that this theme does not use a
        * hard-coded <title> tag in the document head, and expect WordPress to
        * provide it for us.
        */
        add_theme_support( 'title-tag' );

        /*
        * Enable support for Post Thumbnails on posts and pages.
        *
        * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
        */
        add_theme_support( 'post-thumbnails' );

        /*
        * Switch default core markup for search form, comment form, and comments
        * to output valid HTML5.
        */
        add_theme_support( 'html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ]);
    }

    public function register_nav_menus() {
        register_nav_menus( array(
            'main' => __( 'Main Menu', 'site' ),
            'footer_nav' => __( 'Footer Menu', 'site' )
        ) );
    }

    public function add_editor_stylesheet() {
        /*
        * This theme styles the visual editor to resemble the theme style,
        * specifically font, colors, and column width.
        */
        add_editor_style( array( 'build/css/editor-style.css' ) );
    }

    public function force_ssl(){
        if ( FORCE_SSL && !is_ssl() )
        {
            wp_redirect('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 301 );
            exit();
        }
    }
}