<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\customposts;

class common {
    public static function get_posts_by($post_type, $settings) {
        $method = $settings->pick_method;

        switch ($method) {
            case 'all_tax':
            return self::get_all_posts($post_type, $settings->{$settings->pick_method});
            break;
            
            case 'pick':
                return $settings->{$settings->pick_method};
                break;
            
            default:
                return self::get_all_posts($post_type);
                break;
        }
    }

    public static function get_all_posts($post_type, $taxonomies = null) {
        $args = [
            'post_type' => $post_type,
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'ASC'
        ];

        if ($taxonomies) {
            $taxonomy = get_term($taxonomies[0])->taxonomy;
            $args['tax_query'] = [
                [
                    'taxonomy' => $taxonomy,
                    'field' => 'term_id',
                    'terms' => $taxonomies
                ]
            ];
        }

        return get_posts($args);
    }

    public static function get_terms_by($taxonomy, $settings, $order, $layout_type, $sub_layout_order = false) {
        $method = $settings->pick_method;
        $post = get_post();

        switch ($method) {
            case 'pick':
                if ($layout_type === 'quick_links') {
                    $term_meta_field = 'flexible_page_content_'. ($order - 1) .'_quick_links_pick';
                } else {
                    $term_meta_field = 'flexible_page_content_'. ($order - 1) .'_locations_locations_layouts_'.$sub_layout_order.'_pick';
                }

                $terms = get_post_meta($post->ID, $term_meta_field, false);

                if (count($terms)) {
                    foreach ($terms[0] as $key => $term) {
                        $terms[$key] = get_term( $term, $taxonomy );
                    }

                    return $terms;
                }
                return [];

                break;
            
            default:
                return self::get_all_terms($taxonomy);
                break;
        }
    }

    public static function get_all_terms($taxonomy) {
        return get_terms( $taxonomy, array(
            'hide_empty' => true,
        ) );
    }

    public static function format_post_date($ugly_date, $weird_format = null) {
        if ($weird_format) {
            $date = \DateTime::createFromFormat($weird_format, $ugly_date);
        } else {
            $date = new \DateTime($ugly_date);
        }
        return $date->format('F jS');
    }
}
