<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\customposts;

class news {
    public static function get_related_news($post_id = null) {
        $tag_ids = [];
        if($post_id)
        {
            $post_tags = get_the_tags($post_id);
            foreach ($post_tags as $tag) {
                $tag_ids[] = $tag->term_id;
            }
        }

        $args = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'paged' => 1,
        ];

        $query = new \WP_Query(array_merge($args, [            
            'posts_per_page' => 10,
            'post__not_in' => [$post_id],
            'tag__in' => $tag_ids
        ]));
        $news = $query->posts;
        
        // if you dont find enough 
        if (count($news) < 5) {
            $query = new \WP_Query(array_merge($args, [
                'posts_per_page' => 5 - count($news),
                'post__not_in' => array_merge($tag_ids, [$post_id])
            ]));

            $news = array_merge($news, $query->posts);
        }
        
        return $news;
    }


    function register() {
        add_action('rest_post_dispatch', function($response) {
            $response->data =  array_map(
                function($n) {
                    if (!is_array($n)) {
                        return $n;
                    }
                    if (!isset($n['type'])) {
                        return $n;
                    }
                    if ($n['type'] == 'post') {
                        $tags = get_the_tags($n['id']);
                        $n['news_tags'] = $tags ? array_column($tags, 'slug') : [];
                        $n['timestamp'] = get_the_time('U', $n['id']);
                        $image_id = get_post_thumbnail_id($n['id']);
                        $n['image'] = ric_get_image($image_id, 'default', '', 'small-moduleimage');
                        $n['date_format'] = get_the_date('F jS', $n['id']);
                        $n['href'] = get_the_permalink($n['id']);

                    }
                    elseif ($n['type'] == 'sy_credits') {
                        $tags = get_the_terms($n['id'], 'sy_credit_tag');
                        $n['news_tags'] = $tags ? array_column($tags, 'slug') : [];
                        $n['timestamp'] = get_the_modified_time('U', $n['id']);
                        $image_id = get_post_thumbnail_id($n['id']);
                        $n['image'] = ric_get_image($image_id, 'default', '', 'small-moduleimage');
                        $n['date_format'] = get_the_modified_date('F jS', $n['id']);
                        $n['href'] = get_the_permalink($n['id']);
                    }
                    
                    return $n;
                },
                $response->data
                
            );

            return $response;
        });
    }

}
