<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc\customposts;

class locations {

    //get the data from the CMS for locations and pass to JS
    public function get_location_data() {
        
        global $wpdb;

        $querystr = "
            SELECT 
            p.post_title,
            p.ID,
            pm1.meta_value as 'latitude',
            pm2.meta_value as 'longitude'

            FROM $wpdb->posts p

            LEFT JOIN $wpdb->postmeta pm1 on pm1.meta_key = 'latitude' and pm1.post_id = p.ID
            LEFT JOIN $wpdb->postmeta pm2 on pm2.meta_key = 'longitude' and pm2.post_id = p.ID
            
            WHERE 
            p.post_status = 'publish' 
            AND p.post_type = 'sy_locations'
            AND p.post_date < NOW()
            ORDER BY p.post_date DESC
        ";

        $location_posts = $wpdb->get_results($querystr, OBJECT);
       
        foreach($location_posts as $location_post) {
            
            
            $location_post->categories = wp_get_post_terms($location_post->ID, 'sy_location_type');
            $location_post->icon = get_field('icon', $location_post->categories[0])['url'];
            $location_post->colour = get_field('icon_background_color', $location_post->categories[0]);
            $location_post->excerpt = get_field('slider_options', $location_post->ID)['excerpt'];
            $location_post->cta = get_field('slider_options', $location_post->ID)['cta'];
            $location_post->link = get_field('google_maps_link', $location_post->ID);
            $location_post->featured_image = '<div ' . ric_get_background(get_post_thumbnail_id($location_post->ID), 'background-image image') . '></div>';
            $location_post->url = get_permalink($location_post->ID);
        }

        return $location_posts;
    }

    public static function get_location_icon_full($location) {
        $location_type = get_the_terms($location, 'sy_location_type')[0];
        $icon = get_field('icon', $location_type)['ID'];
        $icon_background = get_field('icon_background_color', $location_type);
        return (object) [
            'icon' => $icon,
            'name' => $location_type->name,
            'background' => $icon_background,
            'permalink' => get_term_link($location_type->term_id)
        ];
    }
}

