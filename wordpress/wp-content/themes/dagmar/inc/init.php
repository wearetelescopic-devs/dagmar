<?
/**
 * @package Wordpress
 * @subpackage site
*/
namespace inc;

final class init
{
    /**
     * Store al the classes inside and array
     * @return array    Full list of classes
    */ 
    public static function get_services() {
        return [
            // admin\setup::class,             // theme setup, add theme supports etc
            // admin\dashboard::class,         // add/remove blocks from aDMIN dashboard
            // admin\adminstyles::class,       // add styles to aDMIN dashboard 
            // admin\optionspage::class,       // add options page
            customposts::class,       // add custom posts
            // admin\ajaxify::class,           // ajaxify class
            // utils\enqueue::class,           // enqueue all the FRONTEND styles
            // utils\headings::class,          // useful function for avoiding widows
            // utils\acf::class,               // functions for acf admin actions    
            // utils\filesize::class,          // functions to get file details
            // utils\testimonials::class,      // handle quotes and what not    
            // utils\googlemap::class,         // create shortcode for google map  
            // customposts\common::class,      // handle custom posts functions    
            // customposts\locations::class,   // handle sy_locations functions    
            // customposts\news::class,        // handle post functions   
        ];
    }

    /**
     * Loop through all the classes, initialize them, and call the register() method if it exists
     * @return
    */ 
    public static function register_services() {
        foreach ( self::get_services() as $class ) {
            if (class_exists( $class )) {
                $service = self::instantiate( $class );
                if ( method_exists( $service, 'register' ) ) {
                    $service->register();
                }
            } else {
                var_dump([$class,'sorry class not found!!']);
            }
        }
    }

    /**
     * Initialize each class
     * @param class $class      class from the services array
     * @return class instance   instance of the class
    */ 
    private static function instantiate( $class ) {
        $service = new $class();
        return $service;
    }
}