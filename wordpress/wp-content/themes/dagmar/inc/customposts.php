<?
/**
 * @package Wordpress
 * @subpackage WAT 
 */
namespace inc;

class customposts
{
    function __construct() {
        add_action( 'init',[$this, 'init' ]);
        add_action('init', [$this, 'url_rewrite']);
        
        add_filter( 'init', [$this,'custom_rewrite_tag'], 10, 0 );
    }

    function init() {

        register_post_type( 'site_products',
            array(
                'labels' => array(
                    'name' => __('Products'),
                    'singular_name' => __('Product'),
                    'add_new' => __('Add new Product'),
                    'add_new_item' => __('Add a new Product'),
                    'view_item' => __('View Product'),
                    'edit_item' => __('Edit Product'),
                    'search_items' => __('Search Products'),
                    'not_found' => __('No Products found'),
                    'not_found_in_trash' => __('No Products found in trash')
                ),
                'public' => true,
                'has_archive' => false,
                'hierarchical' => false,
                'supports' => array('title'),
                'show_ui' => true,
                'menu_icon'    => 'dashicons-cart',
                'capability_type' => 'post',
                'rewrite' => array('slug' => 'our-products')
                
            )
        );

        if(WP_DEBUG)
        {
            flush_rewrite_rules(true );
        }
    }

    function url_rewrite() {
        add_rewrite_rule('^our-products/([^/]*)/(services|pricing|features)/?', 
        'index.php?post_type=site_products&name=$matches[1]&page_type=$matches[2]', 
        'top');

        if(WP_DEBUG)
        {
            flush_rewrite_rules(true );
        }
    }
    function custom_rewrite_tag( ) {
        add_rewrite_tag('%page_type%', '([^&]+)');
    }

}