<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage site
 * @since 1.0
 * @version 1.0
 */

//if(!WAT_ajaxify::is_ajax()):


 ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
 <head>
 	<?php wp_head(); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
 	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?include(locate_template('partials/favicons.php'));?>
 </head>

 <body <? body_class(); ?>>

 	<header id="header" class="site-header">
	</header>

 	<main> 

<?//endif;?>